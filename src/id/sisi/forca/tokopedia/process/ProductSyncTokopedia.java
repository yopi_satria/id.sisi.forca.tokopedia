package id.sisi.forca.tokopedia.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.Map;

import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.google.gson.Gson;

import id.sisi.forca.tokopedia.response.ResponseData;
import id.sisi.forca.tokopedia.service.TokopediaAuth;
import id.sisi.forca.tokopedia.service.TokopediaShop;
import id.sisi.forca.tokopedia.util.TokopediaUtil;

public class ProductSyncTokopedia extends SvrProcess{

	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String doIt() throws Exception {
		// Sync ke Tokopedia Online Store
		String message = "";
		String shop_domain = "", shop_name = "",  toped_token = "";
		String appID = "";
		
		int id = getAD_Client_ID();		
		String sql = "select * "
					+ "from "+ TokopediaUtil.TABLE_PRODUCT
					+ "WHERE IsActive='Y' "
					+ "AND TOPED_PRODUCT_ID=? ";		
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, getRecord_ID());
			
			rs = pstmt.executeQuery();
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			
			if (rs.next()) {
				map.put("name", rs.getString("name"));
				map.put("condition", "NEW");
				map.put("description", rs.getString("description"));
				map.put("sku", rs.getString("sku"));
				map.put("price", rs.getDouble("price"));
				map.put("status", "LIMITED");
				
				map.put("stock", rs.getBigDecimal("qtyavailable"));
				map.put("min_order", rs.getString("qtytoorder"));
				map.put("category_id", Integer.parseInt(rs.getString("category")) );
				//map.put("dimension", "");
				//map.put("custom_product_logistics", "");
				//map.put("annotations", "");
				map.put("price_currency", rs.getString("currency"));
				map.put("weight", rs.getString("currency"));
				map.put("weight_unit", "GR");
				map.put("is_free_return", false);
				map.put("is_must_insurance", false);
				//map.put("etalase", "");
				//map.put("pictures", "");
				//map.put("wholesale", "");
			}
			Map<String, Object> mapParam = new LinkedHashMap<String, Object>();
			mapParam.put("products", map);
			
			Gson gson = new Gson();
	        String gsonString = gson.toJson(mapParam);
	        System.out.println(gsonString);
	        
			
			//cek jika expired akan dirubah otomatis
//			toped_token = TokopediaAuth.getToken(appID, null);
//			
//			if(toped_token.length()>0 && shop_domain.length()>0 ) {
//				ResponseData rsData = (ResponseData) TokopediaShop.shopInfo(appID, toped_token, shop_domain);
//				if(rsData.getCodestatus().equalsIgnoreCase("S")) {
//					Map<String, Object> map = (Map<String, Object>) rsData.getResultdata();
////					for (Map.Entry<String,Object> entry : map.entrySet())
////					{    System.out.println("Key = " + entry.getKey() +
////			                             ", Value = " + entry.getValue());
////					}
//					ResponseData writeData = (ResponseData) insertDataShop(map, shop_name);
//					message = writeData.getMessage();
//					System.out.println("jadi shop_id nya :" +map.get("shop_id"));
//				}
//			}
			
		}catch (Exception e) {
			e.printStackTrace();
			message = "Failed to Sync Tokopedia Product";
		}finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		
		return message;
	}
	

}
