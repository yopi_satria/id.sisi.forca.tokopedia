package id.sisi.forca.tokopedia.process;

import org.compiere.model.MSysConfig;
import org.compiere.process.SvrProcess;

import id.sisi.forca.tokopedia.service.TokopediaAuth;
import id.sisi.forca.tokopedia.util.TokopediaUtil;


public class ProcessTokopediaToken extends SvrProcess{

	@Override
	protected void prepare() {
		int AD_Client_ID = 0;
		int AD_Org_ID = 0;
	}

	@Override
	protected String doIt() throws Exception {
		String hasil = "";
		//hasil = TokopediaAuth.testGetToken();
		hasil = TokopediaAuth.testJaxRSToken();
		System.out.println(hasil);
		return hasil;
	}
	

}
