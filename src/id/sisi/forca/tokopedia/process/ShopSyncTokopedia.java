package id.sisi.forca.tokopedia.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import id.sisi.forca.tokopedia.response.ResponseData;
import id.sisi.forca.tokopedia.service.TokopediaAuth;
import id.sisi.forca.tokopedia.service.TokopediaShop;
import id.sisi.forca.tokopedia.util.TokopediaUtil;

public class ShopSyncTokopedia extends SvrProcess {

	@Override
	protected void prepare() {
				
	}

	@Override
	protected String doIt() throws Exception {
		//getinfo shop
		String message = "";
		String shop_domain = "", shop_name = "",  toped_token = "";
		String appID = "";
		
		int id = getAD_Client_ID();		
		String sql = "select a.toped_domain, a.value as shop_name, "
					+ "b.toped_token, b.value as app_id "
					+ "from "+ TokopediaUtil.TABLE_SHOP +" a "
					+ "left join "+ TokopediaUtil.TABLE_APP +" b on a.toped_app_id = b.toped_app_id "
					+ "WHERE a.IsActive='Y' "
					+ "AND a.TOPED_SHOP_ID=? ";		
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, getRecord_ID());
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				shop_domain  = rs.getString("toped_domain");
				shop_name = rs.getString("shop_name");
				toped_token =  rs.getString("toped_token");
				appID = rs.getString("app_id");
			}				
//			System.out.println(shop_domain);
//			System.out.println(shop_name);
//			System.out.println(toped_token);
//			System.out.println(appID);
			
			//cek jika expired akan dirubah otomatis
			toped_token = TokopediaAuth.getToken(appID, null);
			
			if(toped_token.length()>0 && shop_domain.length()>0 ) {
				ResponseData rsData = (ResponseData) TokopediaShop.shopInfo(appID, toped_token, shop_domain);
				if(rsData.getCodestatus().equalsIgnoreCase("S")) {
					Map<String, Object> map = (Map<String, Object>) rsData.getResultdata();
//					for (Map.Entry<String,Object> entry : map.entrySet())
//					{    System.out.println("Key = " + entry.getKey() +
//			                             ", Value = " + entry.getValue());
//					}
					ResponseData writeData = (ResponseData) insertDataShop(map, shop_name);
					message = writeData.getMessage();
					System.out.println("jadi shop_id nya :" +map.get("shop_id"));
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			message = "Gagal Sync Tokopedia Shop";
		}finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		
		return message;
	}
	

	private Object insertDataShop(Map<String, Object> map, String shop_name) {
		ResponseData result = new ResponseData();
			
		String sql = "UPDATE "+ TokopediaUtil.TABLE_SHOP +" SET "
				+ "TOPED_ID_Shop=? , Email=?, "
				+ "url=? , Status=? "
				+ "WHERE IsActive='Y' AND Value=? ";
		PreparedStatement pstmt = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, Integer.parseInt(map.get("shop_id").toString()) );
			pstmt.setString(2, "");
			pstmt.setString(3, map.get("shop_url").toString());
			pstmt.setString(4, "CO");
			pstmt.setString(5, shop_name);
			
			pstmt.executeUpdate();
			result.setCodestatus("S");
			result.setMessage("Success Update Data into Database");
			
		} catch (Exception e) {
			result.errorResponse("Failed insert Data into Database");
			 e.printStackTrace();
		}finally {
			DB.close(pstmt);			
			pstmt = null;
		}
				
		return result;
	}
}
