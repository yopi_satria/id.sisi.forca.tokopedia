package id.sisi.forca.tokopedia.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.json.JSONArray;
import org.json.JSONObject;

import id.sisi.forca.tokopedia.response.ResponseData;
import id.sisi.forca.tokopedia.service.TokopediaAuth;

public class RefreshTokenTokopedia extends SvrProcess{
	
	private String 	p_App_ID = "";  
	private String 	p_Client_ID = ""; 
	private String 	p_Client_Secret = ""; 

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		/*
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("Value"))
				p_App_ID = (String)para[i].getParameter();	
			else if (name.equals("TOPED_ID_CLIENT"))
				p_Client_ID = (String)para[i].getParameter();
			else if (name.equals("TOPED_CLIENT_SECRET"))
				p_Client_Secret = (String)para[i].getParameter();
		}		
		*/
	}

	@Override
	protected String doIt() throws Exception {
//		System.out.println(getAD_PInstance_ID());
//		System.out.println(getRecord_ID());
//		System.out.println(getTable_ID());
				
		String appID="",output="";
		Timestamp expd = null;		
		String token= "",clientid = null,clientsecret = null;
		
		int id = getAD_Client_ID();		
		ResponseData resp = new ResponseData();
//		Long curentdate = System.currentTimeMillis();	
		Timestamp timeToday = new Timestamp(new Date().getTime());
//		
//	    System.out.println(timeToday);     
//	    
//	    Calendar cal = Calendar.getInstance();
//	    cal.setTimeInMillis(timeToday.getTime());
	    		
		
		String sql = "SELECT * FROM Toped_App WHERE IsActive='Y' "
				+ "AND AD_Client_ID=0 AND TOPED_APP_ID=? ";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, getRecord_ID());
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				token  = rs.getString("toped_token");
				expd = rs.getTimestamp("expired");
				clientid = rs.getString("toped_id_client");
				clientsecret = rs.getString("toped_client_secret");
				appID = rs.getString("value");
			}				
			
			//System.out.println(expd.getTime()/1000);
			
			if(token.isBlank() || timeToday.after(expd) ) {									
				resp = (ResponseData) TokopediaAuth.generateToken(appID, clientid, clientsecret);		
				if(resp.getCodestatus().equalsIgnoreCase("S")) {
//					System.out.println(resp.getMessage());
//					JSONObject json = new JSONObject(resp.getMessage());					
////					JSONArray hsl = json.toJSONArray(json.names());
//					System.out.println("token :" + json.getString("access_token"));
//					System.out.println("expires_in :" + json.getDouble("expires_in"));
//					System.out.println("token_type :" + json.getString("token_type"));
//					
//					cal.add(Calendar.HOUR, 24);
//					timeToday = new Timestamp(cal.getTime().getTime());
//					
//					ResponseData writedata = (ResponseData) TokopediaAuth.updateToken(appID, 
//							json.getString("access_token"),
//							timeToday);
//				    if(writedata.getCodestatus().equalsIgnoreCase("E")) 
//				    	output = writedata.getMessage();
//				    else
				    	output = "Berhasil update Token";
				}else {
					System.out.println(resp.getMessage());
					output = resp.getMessage();
				}
				
			}else {
				output = "Token sudah uptodate";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
//		System.out.println(clientid);
//		System.out.println(token);
		
		
		
		return output;
	}
	
}
