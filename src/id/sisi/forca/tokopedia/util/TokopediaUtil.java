package id.sisi.forca.tokopedia.util;

import org.adempiere.base.Core;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;

public class TokopediaUtil {
	
	private final static CLogger s_log = CLogger.getCLogger(TokopediaUtil.class);
	
	public static String APP_ID = "TOKOPEDIA_APP_ID";// = "15486";
	public static String URL_TRANS = "TOKOPEDIA_URL";// = "https://fs.tokopedia.net/v1";
	public static String URL_AUTH = "TOKOPEDIA_URL_AUTH";// = "https://accounts.tokopedia.com/token?grant_type=client_credentials";
	public static String CLIENT_ID = "TOKOPEDIA_CLIENT_ID";
	public static String CLIENT_SECRET = "TOKOPEDIA_CLIENT_SECRET";
	
	public static String TABLE_APP = "Toped_App";
	public static String TABLE_SHOP = "Toped_Shop";
	public static String TABLE_PRODUCT = "Toped_Product";
	public static String TABLE_IMAGE = "Toped_IMAGE";
	
	public static String tokpedAuthURL()
	{
		return MSysConfig.getValue(URL_AUTH);
	}
	
	public static String tokpedURL()
	{
		return MSysConfig.getValue(URL_TRANS);
	}
	
	public static String tokpedAppID()
	{
		return MSysConfig.getValue(APP_ID);
	}
	
	public static String tokpedBearerTest() {
		String username = MSysConfig.getValue(CLIENT_ID);
		String password = MSysConfig.getValue(CLIENT_SECRET);
		String encodePlain = username + ":" + password;
		String base64Credentials = null;
		try {
			base64Credentials = new String(Base64.getEncoder().encode(encodePlain.getBytes()));
		} catch (Exception e) {
			s_log.log(Level.SEVERE, "Tidak bisa encoding Client ID Tokopedia", e);
		}
		return base64Credentials;
	}
	
	public static String generateBearer(String clientID, String clientSecret) {
		String encodePlain = clientID + ":" + clientSecret;
		String base64Credentials = null;
		try {
			base64Credentials = new String(Base64.getEncoder().encode(encodePlain.getBytes()));
		} catch (Exception e) {
			s_log.log(Level.SEVERE, "Tidak bisa encoding Client ID Tokopedia", e);
		}
		return base64Credentials;	
	}
	
	public static String getToken(String appID) {
		String token = null;
		Date exp = null;
		
		String sql = "SELECT Toped_Token,expired FROM Toped_App WHERE IsActive='Y' "
				+ "AND AD_Client_ID=0 AND value=?  ";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setString(1, appID);
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				token  = rs.getString("toped_token");
				exp = rs.getDate("expired");
			}				
			else
				s_log.warning ("tidak ada data Token di APP ID : " + appID);
			
		} catch (Exception e) {
			s_log.log(Level.SEVERE, sql, e);
		}finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		System.out.println(token);
		System.out.println(exp);
		
		return token;
	}
		
}
