package id.sisi.forca.tokopedia.service;

import id.sisi.forca.tokopedia.response.ResponseData;

public class TokopediaProduct {

	public static Object createProductV2(String appID, String token, String shop_id) {
		//POST
		//{{host}}/v2/products/fs/:fs_id/create?shop_id={{shop_id}}
		ResponseData resp = new ResponseData();
		
		return resp;	
	}
	
	public static Object createProductV3(String appID, String token, String shop_id) {
		//POST
		//{{host}}/v3/products/fs/:fs_id/create?shop_id={{shop_id}}'
		ResponseData resp = new ResponseData();
		
		return resp;	
	}
	
	public static Object editProduct(String appID, String token, String shop_id) {
		//PATCH
		//{{host}}/inventory/v1/fs/:fs_id/product/edit?
		ResponseData resp = new ResponseData();
		
		return resp;	
	}
	
	public static Object createProductStatus(String appID, String token, String shop_id) {
		//GET
		//{{host}}/inventory/v1/fs/:fs_id/product/create/status?shop_id={{shop_id}}&upload_id=5719 
		ResponseData resp = new ResponseData();
		
		return resp;	
	}
	
	public static Object editProductStatus(String appID, String token, String shop_id) {
		//GET
		//{{host}}/inventory/v1/fs/:fs_id/product/edit/status?shop_id={{shop_id}}&upload_id=5775
		ResponseData resp = new ResponseData();
		
		return resp;	
	}
}
