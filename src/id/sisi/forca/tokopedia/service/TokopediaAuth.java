package id.sisi.forca.tokopedia.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.json.JSONObject;

import id.sisi.forca.tokopedia.response.ResponseData;
import id.sisi.forca.tokopedia.util.TokopediaUtil;

public class TokopediaAuth extends TokopediaUtil{
	private final static CLogger s_log = CLogger.getCLogger(TokopediaAuth.class);
	
	public static Object generateToken(String appID, String clientID, String clientSecret) {
		//POST
		//https://accounts.tokopedia.com/token?grant_type=client_credentials
		
		Response response;
		ResponseData resp = new ResponseData();
				
		String urlParameters  = "grant_type=client_credentials";
		byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
		//Integer postDataLength = postData.length;
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(clientID, clientSecret);
  
		
		Client client = ClientBuilder.newClient();
		client.register(feature);
		WebTarget webTarget = client.target(TokopediaUtil.tokpedAuthURL());
		 
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		response = invocationBuilder.post(Entity.html(urlParameters));
		 
//		System.out.println(response.getStatus());
//		System.out.println(response.getStatusInfo());
//		System.out.println(response.readEntity(String.class));		
		
		if(response.getStatus() == Status.OK.getStatusCode())
		{
			//Simpan di Database
			JSONObject json = new JSONObject(response.readEntity(String.class));					
//			JSONArray hsl = json.toJSONArray(json.names());
//			System.out.println("token :" + json.getString("access_token"));
//			System.out.println("expires_in :" + json.getDouble("expires_in"));
//			System.out.println("token_type :" + json.getString("token_type"));
			
			//hari ini
			Timestamp timeToday = new Timestamp(new Date().getTime());			
		    //System.out.println(timeToday);     
		    
		    Calendar cal = Calendar.getInstance();
		    cal.setTimeInMillis(timeToday.getTime());			
			cal.add(Calendar.HOUR, 24);
			timeToday = new Timestamp(cal.getTime().getTime());
			
			ResponseData writedata = (ResponseData) TokopediaAuth.updateToken(appID, 
					json.getString("access_token"),
					timeToday);
		    if(writedata.getCodestatus().equalsIgnoreCase("E")) {
		    	resp.setCodestatus("E");
				resp.setMessage("Failed insert token into database");
		    }else {		    	    	
		    	resp.setCodestatus("S");
				resp.setMessage("Success update token into database");
				resp.setValidtoken(json.getString("access_token"));
		    }
			
		}else {			
			resp.setCodestatus("E");
			resp.setMessage(response.getStatusInfo().toString());
			
		}
//		if (response.getStatus() == Status.NOT_FOUND.getStatusCode()) {			
//		}
        
		return resp;
	}
		
	public static Object updateToken(String appID, String token, Timestamp expired) {
		ResponseData resp = new ResponseData();
		
		String sql = "UPDATE "+ TokopediaUtil.TABLE_APP +" SET "
				+ "TOPED_Token=? , Expired=? "
				+ "WHERE IsActive='Y' AND Value=? ";
		PreparedStatement pstmt = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setString(1, token);
			pstmt.setTimestamp(2, expired);
			pstmt.setString(3, appID);
			
			pstmt.executeUpdate();
			resp.setCodestatus("S");
			resp.setMessage("Berhasil Update Token");
			
		} catch (Exception e) {
			resp.errorResponse("Gagal insert token Tokopedia");
			s_log.log(Level.SEVERE, "Gagal insert token Tokopedia", e);
		}finally {
			DB.close(pstmt);			
			pstmt = null;
		}
		
		return resp;
	}
	
	public static String getToken(String appID, Integer toped_app_id) {
		String validtoken ="";
		Timestamp expd = null;		
		String token = null, clientid = null,clientsecret = null;
		Timestamp timeToday = new Timestamp(new Date().getTime());
		
		String whereclause = (toped_app_id == null) ? "AND Value=?" : "AND TOPED_APP_ID=?";
		
		String sql = "SELECT * FROM Toped_App WHERE IsActive='Y' "
				+ "AND AD_Client_ID=? "+ whereclause;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, 0);
			
			if (toped_app_id == null)
				pstmt.setString(2, appID);				
			else
				pstmt.setInt(2, toped_app_id);
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				token  = rs.getString("toped_token");
				expd = rs.getTimestamp("expired");
				clientid = rs.getString("toped_id_client");
				clientsecret = rs.getString("toped_client_secret");
				appID = rs.getString("value");
			}
			
			if(token.isBlank() || timeToday.after(expd) ) {	
				ResponseData resp = (ResponseData) TokopediaAuth.generateToken(appID, clientid, clientsecret);	
				if(resp.getCodestatus().equalsIgnoreCase("S")) {
					validtoken = resp.getValidtoken();
				}
					
			}else {
				validtoken = token;
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		} 
		
		return validtoken;
		
	}
	//==========================================================================
	
	public static String testGetToken() {
		String response = null;
		String authorizationHeader = "Basic " + TokopediaUtil.tokpedBearerTest();
		
		String urlParameters  = "grant_type=client_credentials";
		byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
		Integer postDataLength = postData.length;
		try {
			URL url = new URL (TokopediaUtil.tokpedAuthURL());
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", Integer.toString( postDataLength ));
            connection.setRequestProperty  ("Authorization", authorizationHeader);
            connection.setDoOutput(true);
            connection.getOutputStream().write(urlParameters.getBytes("UTF-8"));
            
            InputStream is = connection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			response = sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} 
        
		return response;
	}
	
	public static String testJaxRSToken() {
		Response response;
		String authorizationHeader = "Basic " + TokopediaUtil.tokpedBearerTest();
		
		String urlParameters  = "grant_type=client_credentials";
		byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
		Integer postDataLength = postData.length;
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("0a37f9d4425c408fb1265cc01fb2e352", "49c3f9f478ca49fd8c0d94bf0f8b9b21");
  
		
		Client client = ClientBuilder.newClient();
		client.register(feature);
		WebTarget webTarget = client.target(TokopediaUtil.tokpedAuthURL());
		 
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		response = invocationBuilder.post(Entity.html(urlParameters));
		 
		System.out.println(response.getStatus());
		System.out.println(response.getStatusInfo());
		System.out.println(response.readEntity(String.class));
        
		return response.toString();
	}
}
