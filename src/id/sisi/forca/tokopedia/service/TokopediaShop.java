package id.sisi.forca.tokopedia.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import id.sisi.forca.tokopedia.response.ResponseData;
import id.sisi.forca.tokopedia.util.TokopediaUtil;

public class TokopediaShop {
	
	public static Object shopInfo(String appID, String token, String shopdomain) {
		//GET
		//uri: https://fs.tokopedia.net/v1/shop/fs/15486/shop-info
		
		Response response = null;
		ResponseData resp = new ResponseData();
		String dataToped = "";
		String uriShopinfo = TokopediaUtil.tokpedURL() + "/v1/shop/fs/"+appID+"/shop-info";
		
		System.out.println(uriShopinfo);
		try {
			//HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(clientID, clientSecret);
						
			Client client = ClientBuilder.newClient();
			WebTarget webTarget = client.target(uriShopinfo);
			 
			Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		
			invocationBuilder.header(HttpHeaders.AUTHORIZATION, "Bearer "+token);
			response = invocationBuilder.get();
			 
			System.out.println(response.getStatus());
			System.out.println(response.getStatusInfo());
//			System.out.println(response.readEntity(String.class));
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
        		
		if(response.getStatus() != Status.OK.getStatusCode())
		{
			resp.setCodestatus("E");
			resp.setMessage(response.getStatusInfo().toString());
		}else {			
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			JSONObject json = new JSONObject(response.readEntity(String.class));	
			JSONArray jsonarray = json.getJSONArray("data");
			for(int i=0; i<jsonarray.length(); i++){   
				  JSONObject obj = jsonarray.getJSONObject(i);  
				  System.out.println(obj); 
				  System.out.println(obj.getString("domain")); 
				  System.out.println("shop domain "+ shopdomain); 
				  if(shopdomain.equalsIgnoreCase(obj.getString("domain").toString())) {
					  map = obj.toMap();				  
				  }				
			}
			resp.setResultdata(map);						
			resp.setCodestatus("S");
			resp.setMessage("Success Get Data");
			
		}
		
		return resp;	
	}
	
	/*
	public static Object shopInfoBasic(String appID, String token, String shopname) {
		//GET
		//uri: https://fs.tokopedia.net/v1/shop/fs/15486/shop-info
		
		String response = null;
		ResponseData resp = new ResponseData();
		String uriShopinfo = TokopediaUtil.tokpedURL() + "/v1/shop/fs/"+appID+"/shop-info";
		
		System.out.println(uriShopinfo);
		try {
			URL url = new URL (uriShopinfo);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            //connection.setRequestProperty("Content-Length", Integer.toString( postDataLength ));
            connection.setRequestProperty  ("Authorization", "Bearer " + token);
            connection.setDoOutput(true);
            //connection.getOutputStream().write(urlParameters.getBytes("UTF-8"));
            
            InputStream is = connection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			response = sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} 
        
		 
		System.out.println(response);
		
//		if(response.getStatus() != Status.OK.getStatusCode())
//		{
//			resp.setCodestatus("E");
//			resp.setMessage(response.getStatusInfo().toString());
//		}else {			
//			resp.setCodestatus("S");
//			resp.setMessage(response.readEntity(String.class));
//			
//		}
		
		return resp;	
	}
	*/
}
