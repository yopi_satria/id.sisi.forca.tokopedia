package id.sisi.forca.tokopedia.component;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;

import id.sisi.forca.tokopedia.process.ProcessTokopediaToken;
import id.sisi.forca.tokopedia.process.ProductSyncTokopedia;
import id.sisi.forca.tokopedia.process.RefreshTokenTokopedia;
import id.sisi.forca.tokopedia.process.ShopSyncTokopedia;

public class TokopediaProcessFactory implements IProcessFactory{

	@Override
	public ProcessCall newProcessInstance(String className) {
		ProcessCall process = null;
        Class<?> processClass = null;
        /**
         * Base.plugin
         */
        
        //Testing Process api
        if (className.equals("id.sisi.forca.tokopedia.process.ProcessTokopediaToken")) {
            processClass = ProcessTokopediaToken.class;
        }
        
        //General 
        if (className.equals("id.sisi.forca.tokopedia.process.RefreshTokenTokopedia")) {
            processClass = RefreshTokenTokopedia.class;
        }
        if (className.equals("id.sisi.forca.tokopedia.process.ShopSyncTokopedia")) {
            processClass = ShopSyncTokopedia.class;
        }
        if (className.equals("id.sisi.forca.tokopedia.process.ProductSyncTokopedia")) {
            processClass = ProductSyncTokopedia.class;
        }
        
		if (processClass != null) {
            try {
                process = (ProcessCall) processClass.newInstance();
                return process;
            } catch (Exception ex) {
                return null;
            }
        }
		return null;
	}

}
