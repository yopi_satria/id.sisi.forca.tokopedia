package id.sisi.forca.tokopedia.response;

import java.util.ArrayList;

public class ResponseData {
	Object resultdata = new ArrayList<Object>();
    
	//E=Error or S=Sukses
	String codestatus = "E";

	String message = "Failed Error Found !!";
	
	String validtoken = "";
	
	public String getValidtoken() {
		return validtoken;
	}

	public void setValidtoken(String validtoken) {
		this.validtoken = validtoken;
	}

	//@XmlElement(name = "codestatus")
	public String getCodestatus() {
		return codestatus;
	}

	public void setCodestatus(String codestatus) {
		this.codestatus = codestatus;
	}
		
	//@XmlElement(name = "message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	//@XmlElement(name = "resultdata")
	public Object getResultdata() {
		return resultdata;
	}

	public void setResultdata(Object resultdata) {
		this.resultdata = resultdata;
	}
	
	public static ResponseData successResponse(String msg, Object maplist) {
        ResponseData respon = new ResponseData();

        respon.setCodestatus("S");
        respon.setMessage("Succeed, " + msg);
        respon.setResultdata(maplist);
        return respon;
    }

	public static ResponseData errorResponse(String msg) {
        ResponseData respon = new ResponseData();

        respon.setCodestatus("E");
        respon.setMessage("Error caused by : " + msg);
        return respon;
    }
	
    public static ResponseData errorResponse(String msg, Object maplist) {
        ResponseData respon = new ResponseData();

        respon.setCodestatus("E");
        respon.setMessage("Error caused by : " + msg);
        respon.setResultdata(maplist);
        return respon;
    }
    
    
}
